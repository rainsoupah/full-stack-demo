from flask import Flask, render_template, request, redirect, url_for
import requests
import json
import requests_cache
import asyncio

app = Flask(__name__)

requests_cache.install_cache('cache')

async def get_data_coroutine(travel_data):
	url = 'https://experimentation.snaptravel.com/interview/hotels'
	headers = {'Content-Type': 'application/json'}
	res = {}
	for data in travel_data:
		resp = await asyncio.coroutine(requests.post)(url, json.dumps(data), headers=headers)
		res[data['provider']] = resp.json().get('hotels')
	return res

def fetch_data(travel_data):
	try:
		return asyncio.get_event_loop().run_until_complete(get_data_coroutine(travel_data))
	except:
		loop = asyncio.new_event_loop()
		asyncio.set_event_loop(loop)
		return loop.run_until_complete(get_data_coroutine(travel_data))

@app.route('/', methods=['POST', 'GET'])
def form():
	city = request.form.get('city')
	checkin = request.form.get('checkin')
	checkout = request.form.get('checkout')

	if city and checkin and checkout:
		snaptravel = { 'provider': 'snaptravel' }
		retail = { 'provider': 'retail' }
		params = {
			'city' : city,
			'checkin' : checkin,
			'checkout' : checkout,
		}
		result = []

		data = fetch_data([{**params, **snaptravel}, {**params, **retail}])

		snap_hotel_by_id = { place.get('id'): place for place in data.get('snaptravel')}
		booking_hotel_by_id = { place.get('id'): place for place in data.get('retail')}


		for id in snap_hotel_by_id.keys():
			if booking_hotel_by_id.get(id) is not None:
				booking_price = { 'booking_price': booking_hotel_by_id[id].get('price') }
				result.append({**snap_hotel_by_id.get(id), **booking_price})
		return render_template('hotels.html', result=result)
	return render_template('demo.html', name='world')


